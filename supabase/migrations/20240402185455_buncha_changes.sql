alter table "public"."deployed_apps" alter column "app_type" set not null;

alter table "public"."deployed_apps" alter column "private_cloud" set not null;

alter table "public"."deployed_apps" alter column "status" set not null;



alter table "public"."app_types" add column "app_subdomain" text not null;

CREATE UNIQUE INDEX app_types_app_subdomain_key ON public.app_types USING btree (app_subdomain);

alter table "public"."app_types" add constraint "app_types_app_subdomain_key" UNIQUE using index "app_types_app_subdomain_key";



drop policy "users manage their own apps" on "public"."deployed_apps";

create table "public"."user_private_cloud" (
    "created_at" timestamp with time zone not null default now(),
    "user_id" uuid not null,
    "user_email" text not null,
    "private_cloud_id" bigint not null
);


drop policy "Enable users full access to their own private cloud" on "public"."private_clouds";

alter table "public"."private_clouds" drop constraint "private_clouds_user_id_key";

alter table "public"."private_clouds" drop constraint "public_private_clouds_user_id_fkey";

drop index if exists "public"."private_clouds_user_id_key";

alter table "public"."private_clouds" drop column "user_id";

set check_function_bodies = off;

CREATE OR REPLACE FUNCTION public.check_subdomain_uniqueness(new_user_id uuid, new_private_cloud_id bigint)
 RETURNS boolean
 LANGUAGE plpgsql
 SECURITY DEFINER
AS $function$
BEGIN
    -- Check if there is another row in the database with the same subdomain
    IF EXISTS (SELECT 1 FROM user_private_cloud WHERE private_cloud_id = check_subdomain_uniqueness.new_private_cloud_id) THEN
        -- Another row with the same subdomain exists, check if the user_id is the same
        IF EXISTS (SELECT 1 FROM user_private_cloud WHERE private_cloud_id = check_subdomain_uniqueness.new_private_cloud_id AND user_id = auth.uid()) THEN
            -- User_id is the same, return true
            RETURN true;
        ELSE
            -- User_id is different, return false
            RETURN false;
        END IF;
    ELSE
        -- No other row with the same subdomain, return true
        -- also need to add check to make sure that the email is the same
        IF new_user_id = auth.uid() THEN
            RETURN true;
        ELSE
            RETURN false;
        END IF;
    END IF;
END;
$function$
;

CREATE OR REPLACE FUNCTION public.ser_private_cloud_ids()
 RETURNS SETOF text
 LANGUAGE plpgsql
 SECURITY DEFINER
AS $function$
DECLARE
    col_value text;
BEGIN
    FOR col_value IN
        SELECT private_cloud_id
        FROM user_private_cloud
        WHERE user_id = auth.uid() -- Assuming there's a column named user_id
    LOOP
        RETURN NEXT col_value;
    END LOOP;
    RETURN;
END;
$function$
;

CREATE OR REPLACE FUNCTION public.user_private_cloud_ids()
 RETURNS SETOF bigint
 LANGUAGE plpgsql
 SECURITY DEFINER
AS $function$
DECLARE
    col_value int8;
BEGIN
    FOR col_value IN
        SELECT private_cloud_id
        FROM user_private_cloud
        WHERE user_id = auth.uid() -- Assuming there's a column named user_id
    LOOP
        RETURN NEXT col_value;
    END LOOP;
    RETURN;
END;
$function$
;

create policy "authenticated users can insert new rows"
on "public"."private_clouds"
as permissive
for insert
to authenticated
with check (true);


create policy "Enable insert for authenticated users only"
on "public"."user_private_cloud"
as permissive
for insert
to authenticated
with check (check_subdomain_uniqueness(user_id, private_cloud_id));


create policy "Enable users to read all records about their own private clouds"
on "public"."user_private_cloud"
as permissive
for select
to authenticated
using ((private_cloud_id IN ( SELECT user_private_cloud_ids.user_private_cloud_ids
   FROM user_private_cloud_ids() user_private_cloud_ids(user_private_cloud_ids))));


alter table "public"."user_private_cloud" enable row level security;

CREATE UNIQUE INDEX user_private_cloud_pkey ON public.user_private_cloud USING btree (user_id, private_cloud_id);

alter table "public"."user_private_cloud" add constraint "user_private_cloud_pkey" PRIMARY KEY using index "user_private_cloud_pkey";

alter table "public"."user_private_cloud" add constraint "public_user_private_cloud_private_cloud_id_fkey" FOREIGN KEY (private_cloud_id) REFERENCES private_clouds(id) not valid;

alter table "public"."user_private_cloud" validate constraint "public_user_private_cloud_private_cloud_id_fkey";

alter table "public"."user_private_cloud" add constraint "public_user_private_cloud_user_id_fkey" FOREIGN KEY (user_id) REFERENCES auth.users(id) not valid;

alter table "public"."user_private_cloud" validate constraint "public_user_private_cloud_user_id_fkey";


create policy "allow users to see their own apps"
on "public"."deployed_apps"
as permissive
for select
to authenticated
using ((EXISTS ( SELECT 1
   FROM user_private_cloud pc
  WHERE ((pc.private_cloud_id = deployed_apps.private_cloud) AND (pc.user_id = auth.uid())))));


create policy "allow users to update their own apps"
on "public"."deployed_apps"
as permissive
for update
to authenticated
using ((EXISTS ( SELECT 1
   FROM user_private_cloud pc
  WHERE ((pc.private_cloud_id = deployed_apps.private_cloud) AND (pc.user_id = auth.uid())))))
with check (true);


create policy "enable users to add new apps to their PC"
on "public"."deployed_apps"
as permissive
for insert
to authenticated
with check ((EXISTS ( SELECT 1
   FROM user_private_cloud pc
  WHERE ((pc.private_cloud_id = deployed_apps.private_cloud) AND (pc.user_id = auth.uid())))));
