alter table "public"."user_private_cloud" add column "customer_id" text;

CREATE UNIQUE INDEX user_private_cloud_customer_id_key ON public.user_private_cloud USING btree (customer_id);

alter table "public"."user_private_cloud" add constraint "user_private_cloud_customer_id_key" UNIQUE using index "user_private_cloud_customer_id_key";



