-- supabase/seed.sql
--
-- create test users
INSERT INTO
    auth.users (
        instance_id,
        id,
        aud,
        role,
        email,
        encrypted_password,
        email_confirmed_at,
        recovery_sent_at,
        last_sign_in_at,
        raw_app_meta_data,
        raw_user_meta_data,
        created_at,
        updated_at,
        confirmation_token,
        email_change,
        email_change_token_new,
        recovery_token
    ) (
        select
            '00000000-0000-0000-0000-000000000000',
            uuid_generate_v4 (),
            'authenticated',
            'authenticated',
            'user' || (ROW_NUMBER() OVER ()) || '@example.com',
            crypt ('password123', gen_salt ('bf')),
            current_timestamp,
            current_timestamp,
            current_timestamp,
            '{"provider":"email","providers":["email"]}',
            '{}',
            current_timestamp,
            current_timestamp,
            '',
            '',
            '',
            ''
        FROM
            generate_series(1, 10)
    );

-- test user email identities
INSERT INTO
    auth.identities (
        id,
        user_id,
        -- New column
        provider_id,
        identity_data,
        provider,
        last_sign_in_at,
        created_at,
        updated_at
    ) (
        select
            uuid_generate_v4 (),
            id,
            -- New column
            id,
            format('{"sub":"%s","email":"%s"}', id :: text, email) :: jsonb,
            'email',
            current_timestamp,
            current_timestamp,
            current_timestamp
        from
            auth.users
    );


-- add matrix app type
INSERT INTO "public"."app_types" ("name", "description", "app_subdomain")
VALUES ('matrix', 'an end-to-end encrypted team communication platform', 'chat');

-- kinda hacky script to make a test private cloud and app
-- DO $$
-- DECLARE
--     user_id_var UUID;
    -- cloud_id int8;
-- BEGIN
    -- SELECT id INTO user_id_var
    -- FROM auth.users
    -- WHERE email = 'user1@example.com';

    -- Now you can use the user_id variable in subsequent SQL statements
    -- INSERT INTO public.private_clouds (user_id, subdomain)
    -- VALUES (user_id_var, 'lucas');
-- 
    -- SELECT id INTO cloud_id
    -- FROM public.private_clouds
    -- WHERE user_id = user_id_var;
-- 
    -- INSERT INTO public.deployed_apps (status, message, private_cloud, app_type)
    -- VALUES ('PENDING_INSTALL', 'this a message', cloud_id, 1);
-- 
-- END $$;
-- 
-- 
-- DO $$
-- DECLARE
    -- user_id_var UUID;
    -- cloud_id int8;
-- BEGIN
    -- SELECT id INTO user_id_var
    -- FROM auth.users
    -- WHERE email = 'user2@example.com';
-- 
    -- -- Now you can use the user_id variable in subsequent SQL statements
    -- INSERT INTO public.private_clouds (user_id, subdomain)
    -- VALUES (user_id_var, 'user2');
-- 
    -- SELECT id INTO cloud_id
    -- FROM public.private_clouds
    -- WHERE user_id = user_id_var;
-- 
    -- INSERT INTO public.deployed_apps (status, message, private_cloud, app_type)
    -- VALUES ('PENDING_INSTALL', 'this a message', cloud_id, 1);
-- 
-- END $$;
